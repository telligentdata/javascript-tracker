/*
 * JavaScript tracker core for Telligent: core.js
 * 
 * Forked from Snowplow Analytics.
 * 
 * Copyright (c) 2014 Snowplow Analytics Ltd. All rights reserved.
 *
 * This program is licensed to you under the Apache License Version 2.0,
 * and you may not use this file except in compliance with the Apache License Version 2.0.
 * You may obtain a copy of the Apache License Version 2.0 at http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the Apache License Version 2.0 is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Apache License Version 2.0 for the specific language governing permissions and limitations there under.
 */

var payload = require('./payload.js');
var uuid = require('uuid');

/**
 * Create a tracker core object
 *
 * @param callback function Function applied to every payload dictionary object
 * @return object Tracker core
 */
function trackerCore(callback) {

	// List of metadata objects which get added to every payload
	var staticMetadata = [];

	/**
	 * Set a persistent key-value pair to be added to every payload
	 *
	 * @param string key Field name
	 * @param Object value Field value
	 * @param string metadataGroup Optional group name to store the static metadata under
	 */
	function addStaticMetadata(name, value, metadataGroup) {
		staticMetadata.push({
			name: name,
			value: value,
			metadataGroup: metadataGroup
		});
	}


	function addStaticMetadataObject(name, object, metadataGroup) {
		staticMetadata.push({
			name: name,
			data: object,
			metadataGroup: metadataGroup
		})
	}

	/**
	 * Returns a copy of a JSON with undefined and null properties removed
	 *
	 * @param object eventJson JSON to clean
	 * @param object exemptFields Set of fields which should not be removed even if empty
	 * @return object A cleaned copy of eventJson
	 */
	function removeEmptyProperties(eventJson, exemptFields) {
		var ret = {};
		exemptFields = exemptFields || {};
		for (var k in eventJson) {
			if (exemptFields[k] || (eventJson[k] !== null && typeof eventJson[k] !== 'undefined')) {
				ret[k] = eventJson[k];
			}
		}
		return ret;
	}

	/**
	 * Merges an array of metadata objects into a payload object
	 *
	 * @param array metadata Array of metadata objects to merge
	 * @return object Metadata object with metadata grouped by metadataGroup
	 */
	function mergeMetadata(metadata) {
		var allMetadata = staticMetadata.concat(metadata || []);

		grouped = {}
		if (allMetadata) {
			for (var i in allMetadata) {
				var metadatum = allMetadata[i];
				var metadataGroup = (grouped.hasOwnProperty(metadatum.metadataGroup) ? grouped[metadatum.metadataGroup] : {});

				if (metadatum.hasOwnProperty("data")) {
					for (var k in metadatum.data) {
						if (metadatum.data.hasOwnProperty(k)) {
							metadataGroup[k] = metadatum.data[k];
						}
					}
				} else {
					metadataGroup[metadatum.name] = metadatum.value;
				}

				grouped[metadatum.metadataGroup] = metadataGroup;
			}
		}

		return grouped;
	}

	/**
	 * Log an event
	 * Adds metadata and StaticMetadata name-value pairs to the payload
	 * Applies the callback to the built payload 
	 *
	 * @param string eventtype Event name/type to be tracked
	 * @param object ctx Data to be attached to the event
	 * @param array metadata Array of custom metadata (headers, user_info) relating to the event
	 * @param number tstamp Timestamp of the event
	 * @return object Payload after the callback is applied
	 */
	function track(eventType, ctx, metadata, tstamp) {
		var pb = payload.payloadBuilder();

		// Add both the eventType and ctx fields
		pb.add('type', eventtype);
		pb.add('ctx', removeEmptyProperties(ctx));

		// Add event ID
		pb.add('event_id', uuid.v4());

		// Add client timestamp
		pb.add('client_tstamp', tstamp || new Date().getTime());

		// Add all metadata to the event
		pb.copyDict(mergeMetadata(metadata));

		if (typeof callback === 'function') {
			callback(pb);
		}

		return pb;
	}

	return {

		/**
		 * Turn base 64 encoding on or off
		 *
		 * @param boolean encode key Field name
		 */
		setBase64Encoding: function (encode) {
			base64 = encode;
		},

		addStaticMetadataValue : addStaticMetadataValue,

		addStaticMetadata: addStaticMetadata,

		/**
		 * Set the tracker version
		 *
		 * @param string version
		 */
		setTrackerVersion: function (version) {
			addStaticMetadata('telligent_api_version', version, 'header');
		},

		/**
		 * Set the event source (app or website)
		 *
		 * @param string source
		 */	
		
		setSource: function(source) {
			addToStaticMetadata('source', source, 'header');
		}

		/**
		 * Set the environment of the tracker (dev, prod)
		 *
		 * @param string environment
		 */	
		
		setEnvironment: function(environment) {
			addToStaticMetadata('env', environment, 'header');
		}		

		/**
		 * Set the application ID
		 *
		 * @param string appId
		 */
		setAppId: function (appId) {
			addToStaticMetadata('app_id', appId, 'header');
		},

		/**
		 * Set the IP address
		 *
		 * @param string appId
		 */
		setIpAddress: function (ip) {
			addToStaticMetadata('ip', ip, 'header');
		},

		/**
		 * Set the platform
		 *
		 * @param string value
		 */
		setPlatform: function (value) {
			addToStaticMetadata('platform', value, 'device_info')
		},

		/**
		 * Set the user ID
		 *
		 * @param string userId
		 */
		setUserId: function (userId) {
			addToStaticMetadata('user_id', userId, 'user_info');
		},

		/**
		 * Set the screen resolution
		 *
		 * @param number width
		 * @param number height
		 */
		setScreenResolution: function (width, height) {
			addToStaticMetadata('screen_resolution', width + 'x' + height, 'device_info');
		},

		/**
		 * Set the viewport dimensions
		 *
		 * @param number width
		 * @param number height
		 */
		setViewport: function (width, height) {
			addToStaticMetadata('viewport_dimensions', width + 'x' + height, 'device_info');
		},

		/**
		 * Set the color depth
		 *
		 * @param number depth
		 */
		setColorDepth: function (depth) {
			addToStaticMetadata('color_depth', depth, 'device_info');
		},

		/**
		 * Set the timezone
		 *
		 * @param string timezone
		 */
		setTimezone: function (timezone) {
			addToStaticMetadata('timezone', timezone, 'device_info');
		},

		/**
		 * Set the language
		 *
		 * @param string lang
		 */
		setLang: function (lang) {
			addToStaticMetadata('locale', lang, 'user_info');
		},

		track: track,

		/**
		 * Log the page view / visit
		 *
		 * @param string customTitle The user-defined page title to attach to this page view
		 * @param array metadata Custom metadata relating to the event
		 * @param number tstamp Timestamp of the event
		 * @return object Payload
		 */
		trackPageView: function(pageUrl, pageTitle, referrer, metadata, tstamp) {
			var ctx = {
				url = pageUrl,
				title = pageTitle,
				referrer = referrer
			};
			return track('page_view', ctx, metadata, tstamp);
		},

		/**
		 * Log that a user is still viewing a given page
		 * by sending a page ping.
		 *
		 * @param string pageTitle The page title to attach to this page ping
		 * @param minxoffset Minimum page x offset seen in the last ping period
		 * @param maxXOffset Maximum page x offset seen in the last ping period
		 * @param minYOffset Minimum page y offset seen in the last ping period
		 * @param maxYOffset Maximum page y offset seen in the last ping period
		 * @param array metadata Custom metadata relating to the event
		 * @param number tstamp Timestamp of the event
		 * @return object Payload
		 */
		trackPagePing: function (pageUrl, pageTitle, referrer, minXOffset, maxXOffset, minYOffset, maxYOffset, metadata, tstamp) {
			var ctx = {
				url = pageUrl,
				page = pageTitle,
				referrer = referrer,
				min_x_offset = minXOffset,
				max_x_offset = maxXOffset,
				min_y_offset = minYOffset,
				max_y_offset = maxYOffset
			};

			return track('page_ping', ctx, metadata, tstamp);
		},

		/**
		 * Track a structured event
		 *
		 * @param string category The name you supply for the group of objects you want to track
		 * @param string action A string that is uniquely paired with each category, and commonly used to define the type of user interaction for the web object
		 * @param string label (optional) An optional string to provide additional dimensions to the event data
		 * @param string property (optional) Describes the object or the action performed on it, e.g. quantity of item added to basket
		 * @param int|float|string value (optional) An integer that you can use to provide numerical data about the user event
		 * @param array metadata Custom metadata relating to the event
		 * @param number tstamp Timestamp of the event
		 * @return object Payload
		 */
		trackStructEvent: function (category, action, label, property, value, metadata, tstamp) {
			var ctx = {
				category : category,
				action : action,
				label : label,
				property : property,
				value : value
			};

			return track('structured_event', ctx, metadata, tstamp);
		},

		/**
		 * Track an ecommerce transaction
		 *
		 * @param string orderId Required. Internal unique order id number for this transaction.
		 * @param string affiliation Optional. Partner or store affiliation.
		 * @param string total Required. Total amount of the transaction.
		 * @param string tax Optional. Tax amount of the transaction.
		 * @param string shipping Optional. Shipping charge for the transaction.
		 * @param string city Optional. City to associate with transaction.
		 * @param string state Optional. State to associate with transaction.
		 * @param string country Optional. Country to associate with transaction.
		 * @param string currency Optional. Currency to associate with this transaction.
		 * @param array metadata Optional. Metadata relating to the event.
		 * @param number tstamp Optional. Timestamp of the event
		 * @return object Payload
		 */
		trackEcommerceTransaction: function (orderId, affiliation, totalValue, taxValue, shipping, city, state, country, currency, metadata, tstamp) {
			var ctx = {
				order_id: orderId,
				affiliation : affiliation,
				total_value : totalValue,
				tax_value : taxValue,
				shipping : shipping,
				city : city,
				state : state,
				country : country,
				currency : currency
			};

			return track('ecommerce_transaction', ctx, metadata, tstamp);
		},

		/**
		 * Track an ecommerce transaction item
		 *
		 * @param string orderId Required Order ID of the transaction to associate with item.
		 * @param string sku Required. Item's SKU code.
		 * @param string name Optional. Product name.
		 * @param string category Optional. Product category.
		 * @param string price Required. Product price.
		 * @param string quantity Required. Purchase quantity.
		 * @param string currency Optional. Product price currency.
		 * @param array metadata Optional. Metadata relating to the event.
		 * @param number tstamp Optional. Timestamp of the event
		 * @return object Payload
		 */
		trackEcommerceTransactionItem: function (orderId, sku, name, category, price, quantity, currency, metadata, tstamp) {
			var ctx = {
				order_id : orderId,
				sku : sku,
				name : name,
				category : category,
				price : price,
				quantity : quantity,
				currency : currency
			};

			return track('ecommerce_transaction_item', ctx, metadata, tstamp);
		},

		/**
		 * Track a screen view unstructured event
		 *
		 * @param string name The name of the screen
		 * @param string id The ID of the screen
		 * @param number tstamp Timestamp of the event
		 * @param array metadata Optional. Metadata relating to the event.
		 * @return object Payload
		 */
		trackScreenView: function (name, id, metadata, tstamp) {
			var ctx = {
				name : name,
				id : id
			};

			return track('screen_view', ctx, metadata, tstamp);
		},

		/**
		 * Log the link or click with the server
		 *
		 * @param string targetUrl
		 * @param string elementId
		 * @param array elementClasses
		 * @param string elementTarget
		 * @param string elementContent innerHTML of the link
		 * @param array metadata Custom metadata relating to the event
		 * @param number tstamp Timestamp of the event
		 * @return object Payload
		 */
		trackLinkClick:  function (targetUrl, elementId, elementClasses, elementTarget, elementContent, metadata, tstamp) {
			var ctx = {
				target_url : targetUrl,
				element_id : elementId,
				element_classes : elementClasses,
				element_target : elementTarget,
				element_content : elementContent
			};

			return track('link_click', ctx, metadata, tstamp);
		},

		/**
		 * Track an ad being served
		 *
		 * @param string impressionId Identifier for a particular ad impression
		 * @param string costModel The cost model. 'cpa', 'cpc', or 'cpm'			 
		 * @param number cost Cost
		 * @param string bannerId Identifier for the ad banner displayed
		 * @param string zoneId Identifier for the ad zone
		 * @param string advertiserId Identifier for the advertiser
		 * @param string campaignId Identifier for the campaign which the banner belongs to
		 * @param array Custom metadata relating to the event
		 * @param number tstamp Timestamp of the event
		 * @return object Payload
		 */
		trackAdImpression: function(impressionId, costModel, cost, targetUrl, bannerId, zoneId, advertiserId, campaignId, metadata, tstamp) {
			var ctx = {
				impression_id : impressionId,
				cost_model : costModel,
				cost : cost,
				target_url : targetUrl,
				banner_id : bannerId,
				zone_id : zoneId,
				advertiser_id : advertiserId,
				campaign_id : campaignId
			};

			return track('ad_impression', ctx, metadata, tstamp);
		},

		/**
		 * Track an ad being clicked
		 *
		 * @param string clickId Identifier for the ad click
		 * @param string costModel The cost model. 'cpa', 'cpc', or 'cpm'			 
		 * @param number cost Cost
		 * @param string targetUrl (required) The link's target URL
		 * @param string bannerId Identifier for the ad banner displayed
		 * @param string zoneId Identifier for the ad zone
		 * @param string impressionId Identifier for a particular ad impression
		 * @param string advertiserId Identifier for the advertiser
		 * @param string campaignId Identifier for the campaign which the banner belongs to
		 * @param array Custom metadata relating to the event
		 * @param number tstamp Timestamp of the event
		 * @return object Payload
		 */
		trackAdClick: function (targetUrl, clickId, costModel, cost, bannerId, zoneId, impressionId, advertiserId, campaignId, metadata, tstamp) {
			var ctx = {
				target_url : targetUrl,
				click_id : clickId,
				cost_model : costModel,
				cost : cost,
				banner_id : bannerId,
				zone_id : zoneId,
				impression_id : impressionId,
				advertiser_id : advertiserId,
				campaign_id : campaignId
			};

			return track('ad_click', ctx, metadata, tstamp);
		},

		/**
		 * Track an ad conversion event
		 *
		 * @param string conversionId Identifier for the ad conversion event
		 * @param number cost Cost
		 * @param string category The name you supply for the group of objects you want to track
		 * @param string action A string that is uniquely paired with each category
		 * @param string property Describes the object of the conversion or the action performed on it
		 * @param number initialValue Revenue attributable to the conversion at time of conversion
		 * @param string advertiserId Identifier for the advertiser
		 * @param string costModel The cost model. 'cpa', 'cpc', or 'cpm'
		 * @param string campaignId Identifier for the campaign which the banner belongs to
		 * @param array Custom metadata relating to the event
		 * @param number tstamp Timestamp of the event
		 * @return object Payload
		 */
		trackAdConversion: function (conversionId, costModel, cost, category, action, property, initialValue, advertiserId, campaignId, metadata, tstamp) {
			var ctx = {
				conversion_id : conversionId,
				cost_model : costModel,
				cost : cost,
				category : category,
				action : action,
				property : property,
				initial_value : initialValue,
				advertiser_id : advertiserId,
				campauign_id : campaignId
			};

			return track('ad_conversion', ctx, metadata, tstamp);
		},

		/**
		 * Track a social event
		 *
		 * @param string action Social action performed
		 * @param string network Social network
		 * @param string target Object of the social action e.g. the video liked, the tweet retweeted
		 * @param array Custom metadata relating to the event
		 * @param number tstamp Timestamp of the event
		 * @return object Payload
		 */
		trackSocialInteraction: function (action, network, target, metadata, tstamp) {
			var ctx = {
				action : action,
				network : network,
				target : target
			};

			return track('social_interaction', ctx, metadata, tstamp);
		},

		/**
		 * Track an add-to-cart event
		 *
		 * @param string sku Required. Item's SKU code.
		 * @param string name Optional. Product name.
		 * @param string category Optional. Product category.
		 * @param string unitPrice Optional. Product price.
		 * @param string quantity Required. Quantity added.
		 * @param string currency Optional. Product price currency.
		 * @param array metadata Optional. Metadata relating to the event.
		 * @param number tstamp Optional. Timestamp of the event
		 * @return object Payload
		 */
		trackAddToCart: function (sku, name, category, unitPrice, quantity, currency, metadata, tstamp) {
			var ctx = {
				sku : sku,
				name : name,
				category : category,
				unit_price : unitPrice,
				quantity : quantity,
				currency : currency
			};

			return track('add_to_cart', ctx, metadata, tstamp);
		},

		/**
		 * Track a remove-from-cart event
		 *
		 * @param string sku Required. Item's SKU code.
		 * @param string name Optional. Product name.
		 * @param string category Optional. Product category.
		 * @param string unitPrice Optional. Product price.
		 * @param string quantity Required. Quantity removed.
		 * @param string currency Optional. Product price currency.
		 * @param array metadata Optional. Metadata relating to the event.
		 * @param number tstamp Optional. Timestamp of the event
		 * @return object Payload
		 */
		trackRemoveFromCart: function (sku, name, category, unitPrice, quantity, currency, metadata, tstamp) {
			var ctx = {
				sku : sku,
				name : name,
				category : category,
				unit_price : unitPrice,
				quantity : quantity,
				currency : currency
			};

			return track('remove_from_cart', ctx, metadata, tstamp);
		},

		/**
		 * Track the value of a form field changing
		 *
		 * @param string formId The parent form ID
		 * @param string elementId ID of the changed element
		 * @param string nodeName "INPUT", "TEXTAREA", or "SELECT"
		 * @param string type Type of the changed element if its type is "INPUT"
		 * @param array elementClasses List of classes of the changed element
		 * @param string value The new value of the changed element
		 * @param array metadata Optional. Metadata relating to the event.
		 * @param number tstamp Optional. Timestamp of the event
		 * @return object Payload
		 */
		trackFormChange: function(formId, elementId, nodeName, type, elementClasses, value, metadata, tstamp) {
			var ctx = {
				form_id : formId,
				element_id : elementId,
				node_name : nodeName,
				type : type,
				element_classes : elementClasses,
				value : value
			}

			return track('form_change', ctx, metadata, tstamp);
		},

		/**
		 * Track a form submission event
		 *
		 * @param string formId ID of the form
		 * @param array formClasses Classes of the form
		 * @param array elements Mutable elements within the form
		 * @param array metadata Optional. Metadata relating to the event.
		 * @param number tstamp Optional. Timestamp of the event
		 * @return object Payload
		 */
		trackFormSubmission: function(formId, formClasses, elements, metadata, tstamp) {
			var ctx = {
				form_id : formId,
				form_classes : formClasses,
				elements : elements
			};

			return track('form_submission', ctx, metadata, tstamp);
		},

		/**
		 * Track an internal search event
		 *
		 * @param array terms Search terms
		 * @param object filters Search filters
		 * @param totalResults Number of results
		 * @param pageResults Number of results displayed on page
		 * @param array metadata Optional. Metadata relating to the event.
		 * @param number tstamp Optional. Timestamp of the event
		 * @return object Payload
		 */
		trackSiteSearch: function(terms, filters, totalResults, pageResults, metadata, tstamp) {
			var ctx = {
				terms : terms,
				filters : filters,
				total_results : totalResults,
				page_results : pageResults
			};

			return track('site_search', ctx, metadata, tstamp);
		}
	};
};

module.exports = trackerCore;
